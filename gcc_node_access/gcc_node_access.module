<?php

/**
 * @file
 * TODO.
 */

define('GCC_NODE_ACCESS_PUBLIC', 0);
define('GCC_NODE_ACCESS_PRIVATE', 1);

define('GCC_NODE_ACCESS_REALM_GROUP', 'gcc_node_access_group');
define('GCC_NODE_ACCESS_REALM_GROUP_PUBLIC', 'gcc_node_access_public_');
define('GCC_NODE_ACCESS_REALM_GROUP_PRIVATE', 'gcc_node_access_private_');

require_once __DIR__ . '/gcc_node_access.field.inc';

/* Core Hooks Implementations */

/**
 * Implements hook_menu().
 */
function gcc_node_access_menu() {

  $items = array();

  // Global admin.
  $items['admin/config/gcc/node-access'] = array(

    'title' => 'Node access',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gcc_node_access_config'),
    'access arguments' => array('administer gcc configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 4,
  );

  return $items;
}

/**
 * Form builder.
 */
function gcc_node_access_config() {

  $form = array();

  $form['handle'] = array(

    '#type' => 'fieldset',
    '#title' => t('Node Access Handling'),
  );

  $form['handle']['gcc_node_access_association'] = array(

    '#type' => 'checkbox',
    '#title' => t('Handle association with groups'),
    '#default_value' => variable_get('gcc_node_access_association', FALSE),
  );

  $form['handle']['gcc_node_access_view'] = array(

    '#type' => 'checkbox',
    '#title' => t('Handle view access'),
    '#default_value' => variable_get('gcc_node_access_view', FALSE),
    '#description' => '<p>' . t('If you change this setting, you will have to !link.', array(
      '!link' => l(t('rebuild the permissions'), 'admin/reports/status/rebuild', array(
        'query' => array(
          'destination' => 'admin/config/gcc/node-access',
        ),
      )),
    )) . '</p>',
    '#element_validate' => array('_gcc_node_access_flag'),
  );

  $form['handle']['gcc_node_access_view']['#description'] .= '<p>' . t('When this settigns is disabled, anyone will be able to see the private content.') . '</p>';

  $form['handle']['gcc_node_access_edit'] = array(

    '#type' => 'checkbox',
    '#title' => t('Handle edit access'),
    '#default_value' => variable_get('gcc_node_access_edit', FALSE),
  );

  $form['handle']['gcc_node_access_delete'] = array(

    '#type' => 'checkbox',
    '#title' => t('Handle delete access'),
    '#default_value' => variable_get('gcc_node_access_delete', FALSE),
  );

  $form['mode'] = array(

    '#type' => 'fieldset',
    '#title' => t('Node Access Mode'),
  );

  $form['mode']['gcc_node_access_strict'] = array(

    '#type' => 'checkbox',
    '#title' => t('Strict Mode'),
    '#description' => t('When enabled, GCC Node Access will strictly deny access instead of leaving a chance for some other node access modules to allow access.'),
    '#default_value' => variable_get('gcc_node_access_strict', FALSE),
  );

  return system_settings_form($form);
}

/**
 * If the view handling is changed, mark the node access for rebuild.
 */
function _gcc_node_access_flag($element, &$form_state, $form) {

  // If the user change the view handling setting, he need to rebuild access.
  if ($element['#value'] != variable_get('gcc_node_access_view', FALSE)) {
    node_access_needs_rebuild(TRUE);
  }
}

/**
 * Implements hook_node_access_records().
 */
function gcc_node_access_node_access_records($node) {

  // Bug, return nothing.
  if (!isset($node->type)) {
    return array();
  }

  // If GCC node access is not asked to handle the view, do nothing.
  if (!variable_get('gcc_node_access_view', FALSE)) {
    return array();
  }

  $grants = array();

  if (gcc_is_group($node)) {
    $grants[] = array(

      'realm' => GCC_NODE_ACCESS_REALM_GROUP,
      'gid' => $node->nid,
      'grant_view' => 1,
      'grant_update' => 0,
      'grant_delete' => 0,
      'priority' => 0,
    );
  }

  if (gcc_is_group_content($node)) {

    foreach ($node->{GCC_FIELD_AUDIENCE}[LANGUAGE_NONE] as $group) {
      if (gcc_node_access_is_private($node)) {
        $grants[] = array(

          'realm' => GCC_NODE_ACCESS_REALM_GROUP_PRIVATE . $node->type . '_' . $group['entity_type'],
          'gid' => $group['entity_id'],
          'grant_view' => 1,
          'grant_update' => 0,
          'grant_delete' => 0,
          'priority' => 0,
        );
      }
      else {
        $grants[] = array(

          'realm' => GCC_NODE_ACCESS_REALM_GROUP_PUBLIC . $node->type . '_' . $group['entity_type'],
          'gid' => $group['entity_id'],
          'grant_view' => 1,
          'grant_update' => 0,
          'grant_delete' => 0,
          'priority' => 0,
        );
      }
    }
  }

  return $grants;
}

/**
 * Implements hook_node_grants().
 */
function gcc_node_access_node_grants($account, $op) {

  // If GCC node access is not asked to handle the view, do nothing.
  if (!variable_get('gcc_node_access_view', FALSE)) {
    return array();
  }

  $grants = array();

  $groups = gcc_membership_get_by_permission($account->uid, 'view group', TRUE, TRUE, TRUE);
  foreach ($groups as $group) {
    if ($group->entity_type == 'node') {
      $grants[GCC_NODE_ACCESS_REALM_GROUP][] = $group->entity_id;
    }
  }

  $bundles = gcc_features_get_enabled_bundles('group_content');
  if (isset($bundles['node'])) {
    foreach (array_keys($bundles['node']) as $type) {

      $groups = gcc_membership_get_by_permission($account->uid, 'view public ' . $type, TRUE, TRUE, TRUE);
      foreach ($groups as $group) {
        $grants[GCC_NODE_ACCESS_REALM_GROUP_PUBLIC . $type . '_' . $group->entity_type][] = $group->entity_id;
      }

      $groups = gcc_membership_get_by_permission($account->uid, 'view private ' . $type, TRUE, TRUE, TRUE);
      foreach ($groups as $group) {
        $grants[GCC_NODE_ACCESS_REALM_GROUP_PRIVATE . $type . '_' . $group->entity_type][] = $group->entity_id;
      }
    }
  }

  return $grants;
}

/**
 * Implements hook_node_grants_alter().
 */
function gcc_node_access_node_grants_alter(&$grants, $account, $op) {

  // Do nothing if not in strict mode.
  if (!variable_get('gcc_node_access_strict', FALSE)) {
    return;
  }

  // In strict mode, remove all grants except ours.
  foreach ($grants as $realm => $grant) {

    if ($realm == GCC_NODE_ACCESS_REALM_GROUP) {
      continue;
    }

    if (strpos($realm, GCC_NODE_ACCESS_REALM_GROUP_PUBLIC) === 0) {
      continue;
    }

    if (strpos($realm, GCC_NODE_ACCESS_REALM_GROUP_PRIVATE) === 0) {
      continue;
    }
  }
}

/**
 * Implements hook_field_delete_instance().
 *
 * We use this hook instead of gcc_node_access_gcc_features_disable()
 * to mark the node access for rebuild because the user can remove
 * our field without using the GCC API.
 */
function gcc_node_access_field_delete_instance($instance) {

  if ($instance['field_name'] == GCC_NODE_ACCESS_FIELD_PRIVATE) {
    node_access_needs_rebuild(TRUE);
  }
}

/**
 * Implements hook_node_access().
 */
function gcc_node_access_node_access($node, $op, $account) {

  // We only take care of group and group content.
  if (!gcc_is_group($node) && !gcc_is_group_content($node)) {
    return NODE_ACCESS_IGNORE;
  }

  // We cannot grants edit or delete access to anonymous.
  if (!$account->uid) {
    return NODE_ACCESS_IGNORE;
  }

  $perm = NULL;
  switch ($op) {

    case 'update':
      if (!variable_get('gcc_node_access_edit', FALSE)) {
        return NODE_ACCESS_IGNORE;
      }
      $perm = 'edit';
      break;

    case 'delete':
      if (!variable_get('gcc_node_access_delete', FALSE)) {
        return NODE_ACCESS_IGNORE;
      }
      $perm = 'delete';
      break;

    default:
      return NODE_ACCESS_IGNORE;
  }

  // The group permission take precedence.
  if (gcc_is_group($node)) {
    if (gcc_user_access($perm . ' group', 'node', $node, $account)) {
      return NODE_ACCESS_ALLOW;
    }
  }
  else {
    foreach ($node->{GCC_FIELD_AUDIENCE}[LANGUAGE_NONE] as $group) {

      $entity = entity_load($group['entity_type'], array($group['entity_id']));
      $entity = reset($entity);

      if ($entity) {

        if (gcc_user_access($perm . ' any ' . $node->type, $group['entity_type'], $entity, $account)) {
          return NODE_ACCESS_ALLOW;
        }

        if ($node->uid == $account->uid) {
          if (gcc_user_access($perm . ' own ' . $node->type, $group['entity_type'], $entity, $account)) {
            return NODE_ACCESS_ALLOW;
          }
        }
      }
    }
  }

  return variable_get('gcc_node_access_strict', FALSE) ? NODE_ACCESS_DENY : NODE_ACCESS_IGNORE;
}

/* GCC Hooks Implementations */

/**
 * Implements hook_gcc_permission().
 */
function gcc_node_access_gcc_permission() {

  $perm = array();

  if (variable_get('gcc_node_access_view', FALSE)) {
    $perm['view group'] = array(

      'title' => t('View the group'),
      'group' => t('Group Access'),
      'global' => array(
        'bypass node access',
      ),
    );
  }

  if (variable_get('gcc_node_access_edit', FALSE)) {
    $perm['edit group'] = array(

      'title' => t('Edit the group'),
      'group' => t('Group Access'),
      'global' => array(
        'bypass node access',
      ),
    );
  }

  if (variable_get('gcc_node_access_delete', FALSE)) {
    $perm['delete group'] = array(

      'title' => t('Delete the group'),
      'group' => t('Group Access'),
      'global' => array(
        'bypass node access',
      ),
    );
  }

  foreach (node_type_get_types() as $type => $info) {

    // Handle only group content.
    $features = gcc_features_get_bundle_info('node', $type);
    if (!isset($features['group_content']['enabled']) || !$features['group_content']['enabled']) {
      continue;
    }

    if (variable_get('gcc_node_access_association', FALSE)) {

      $perm['associate ' . $type] = array(

        'title' => t('Associate !type to the group', array('!type' => $info->name)),
        'group' => $info->name,
        'global' => array(
          'bypass node access',
        ),
      );
    }

    if (variable_get('gcc_node_access_view', FALSE)) {

      $perm['view public ' . $type] = array(

        'title' => t('View public !type', array('!type' => $info->name)),
        'group' => $info->name,
        'global' => array(
          'bypass node access',
        ),
      );

      $perm['view private ' . $type] = array(

        'title' => t('View private !type', array('!type' => $info->name)),
        'group' => $info->name,
        'global' => array(
          'bypass node access',
        ),
      );
    }

    if (variable_get('gcc_node_access_edit', FALSE)) {

      $perm['edit any ' . $type] = array(

        'title' => t('Edit any !type', array('!type' => $info->name)),
        'group' => $info->name,
        'global' => array(
          'bypass node access',
        ),
      );

      $perm['edit own ' . $type] = array(

        'title' => t('Edit own !type', array('!type' => $info->name)),
        'group' => $info->name,
        'global' => array(
          'bypass node access',
        ),
      );
    }

    if (variable_get('gcc_node_access_delete', FALSE)) {

      $perm['delete any ' . $type] = array(

        'title' => t('Delete any !type', array('!type' => $info->name)),
        'group' => $info->name,
        'global' => array(
          'bypass node access',
        ),
      );

      $perm['delete own ' . $type] = array(

        'title' => t('Delete own !type', array('!type' => $info->name)),
        'group' => $info->name,
        'global' => array(
          'bypass node access',
        ),
      );
    }
  }

  return $perm;
}

/**
 * Implements hook_gcc_audience_allowed_value_alter().
 */
function gcc_node_access_gcc_audience_allowed_value_alter(&$list, $context) {

  global $user;

  if (!variable_get('gcc_node_access_association', FALSE)) {
    return;
  }

  if (user_access('administer all groups')) {
    return;
  }

  $instance = $context['instance'];

  // We operate only on node.
  if (!isset($instance['entity_type']) || !isset($instance['bundle']) || $instance['entity_type'] != 'node') {
    return;
  }

  $list = array();
  $node_type = $instance['bundle'];

  // Fetch all the groups where the user can post.
  $list = gcc_membership_get_by_permission($user->uid, 'associate ' . $node_type, TRUE, TRUE, TRUE);
}

/**
 * Implements hook_gcc_features_info().
 */
function gcc_node_access_gcc_features_info() {

  $features = array();

  $features['private'] = array(

    'label' => t('Enable the private content features'),
    'explaination' => t('Can choose the content visibility'),
  );

  return $features;
}

/**
 * Implements hook_gcc_features_is_enabled().
 */
function gcc_node_access_gcc_features_is_enabled($feature, $entity_type, $bundle) {

  switch ($feature) {

    case 'private':
      return is_array(field_read_instance($entity_type, GCC_NODE_ACCESS_FIELD_PRIVATE, $bundle));
  }
}

/**
 * Implements hook_gcc_features_enabled().
 */
function gcc_node_access_gcc_features_enable($feature, $entity_type, $bundle) {

  switch ($feature) {

    case 'private':
      gcc_field_create_field_instance(GCC_NODE_ACCESS_FIELD_TYPE_PRIVATE, GCC_NODE_ACCESS_FIELD_PRIVATE, t('Content Visibility'), $entity_type, $bundle);

      if (!variable_get('gcc_node_access_view', FALSE)) {
        drupal_set_message(t('You must activate the view acces handling in gcc node access for the private field to be effective. Go to the !link', array('!link' => l(t('configuration form'), 'admin/config/gcc/node-access'))), 'warning');
      }
      break;
  }
}

/**
 * Implements hook_gcc_features_disabled().
 */
function gcc_node_access_gcc_features_disable($feature, $entity_type, $bundle) {

  switch ($feature) {

    case 'private':
      gcc_field_delete_field_instance(GCC_NODE_ACCESS_FIELD_PRIVATE, $entity_type, $bundle);
      break;
  }
}

/* Other */

/**
 * Check of a content is private.
 */
function gcc_node_access_is_private($node) {

  if (!gcc_is_group_content($node)) {
    return FALSE;
  }

  if (isset($node->{GCC_NODE_ACCESS_FIELD_PRIVATE}[LANGUAGE_NONE][0]['value'])) {
    return $node->{GCC_NODE_ACCESS_FIELD_PRIVATE}[LANGUAGE_NONE][0]['value'] != 0;
  }

  return FALSE;
}
