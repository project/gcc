<?php

/**
 * @file
 * TODO.
 */

// Include field definition.
require_once __DIR__ . '/gcc_contextual_block.field.inc';

// Include API functions.
require_once __DIR__ . '/gcc_contextual_block.inc';

define('GCC_CONTEXTUAL_BLOCK_HIDDEN', '__hidden');
define('GCC_CONTEXTUAL_BLOCK_DISABLE', '__disable');

/* Core Hooks Implementations */

/**
 * Implements hook_menu().
 */
function gcc_contextual_block_menu() {

  $items = array();

  // Global admin.
  $items['admin/config/gcc/contextual-block'] = array(

    'title' => 'Contextual Block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gcc_contextual_block_config'),
    'access arguments' => array('administer gcc configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'gcc_contextual_block.admin.inc',
    'weight' => 6,
  );

  $entity_info = entity_get_info();
  foreach ($entity_info as $entity_type => $info) {

    // Skip non GCC Entity.
    if (!isset($info['gcc'])) {
      continue;
    }

    $base = count(explode('/', $info['gcc']['path']));

    $items[$info['gcc']['path'] . '/gcc/contextual-block'] = array(

      'title' => 'Blocks',
      'page callback' => 'drupal_get_form',
      'page arguments' => array(

        'gcc_contextual_block_layout_config',
        $entity_type,
        $info['gcc']['entity position'],
        $info['gcc']['path'],
      ),
      'access callback' => 'gcc_contextual_block_access_admin',
      'access arguments' => array(

        $entity_type,
        $info['gcc']['entity position'],
      ),
      'type' => MENU_LOCAL_TASK,
      'file' => 'gcc_contextual_block.admin.inc',
      'weight' => 6,
    );
  }

  return $items;
}

/**
 * Implements hook_block_list_alter().
 */
function gcc_contextual_block_block_list_alter(&$block_list) {

  global $theme_key;

  $context = gcc_context_get_context();
  if (!$context) {
    return;
  }

  $entity = entity_load($context['entity_type'], array($context['entity_id']));

  if (!isset($entity[$context['entity_id']]->{GCC_CONTEXTUAL_BLOCK_FIELD}[LANGUAGE_NONE][0]['config'])) {
    return;
  }

  $theme = variable_get('theme_default', 'bartik');

  if (module_exists('gcc_theme') && isset($entity[$context['entity_id']]->{GCC_THEME_FIELD_THEME}[LANGUAGE_NONE][0]['value'])) {
    $theme = $entity[$context['entity_id']]->{GCC_THEME_FIELD_THEME}[LANGUAGE_NONE][0]['value'];
  }

  if ($theme != $theme_key) {
    return;
  }

  $config = $entity[$context['entity_id']]->{GCC_CONTEXTUAL_BLOCK_FIELD}[LANGUAGE_NONE][0]['config'];

  $blocks_bid = db_select('block', 'b')
    ->fields('b')
    ->condition('b.theme', $theme_key)
    ->execute()
    ->fetchAllAssoc('bid');

  foreach ($config as $bid => $block) {

    if (!isset($block_list[$bid]) && isset($blocks_bid[$bid])) {

      $block_list[$bid] = $blocks_bid[$bid];
      $block_list[$bid]->status = 1;
    }

    if (isset($block_list[$bid])) {

      foreach ($block as $key => $value) {
        if ($value != GCC_CONTEXTUAL_BLOCK_DISABLE) {
          $block_list[$bid]->{$key} = $value;
        }
      }

      if ($block['region'] == GCC_CONTEXTUAL_BLOCK_HIDDEN) {
        unset($block_list[$bid]);
      }
    }
  }

  uasort($block_list, function ($a, $b) {

    return $a->weight - $b->weight;
  });
}

/* Access Callbacks */

/**
 * Access callback.
 */
function gcc_contextual_block_access_admin($entity_type, $entity) {

  if (!isset($entity->{GCC_CONTEXTUAL_BLOCK_FIELD})) {
    return FALSE;
  }

  return gcc_user_access('administer group', $entity_type, $entity) || gcc_user_access('administer contextual block', $entity_type, $entity);
}

/* GCC Hooks Implementations */

/**
 * Implements hook_gcc_features_info().
 */
function gcc_contextual_block_gcc_features_info() {

  $features = array();

  $features['contextual_block'] = array(

    'label' => t('Enable the contextual block features'),
    'explaination' => t('Can choose a custom block layout'),
  );

  return $features;
}

/**
 * Implements hook_gcc_features_is_enabled().
 */
function gcc_contextual_block_gcc_features_is_enabled($feature, $entity_type, $bundle) {

  switch ($feature) {

    case 'contextual_block':
      return is_array(field_read_instance($entity_type, GCC_CONTEXTUAL_BLOCK_FIELD, $bundle));
  }
}

/**
 * Implements hook_gcc_features_enabled().
 */
function gcc_contextual_block_gcc_features_enable($feature, $entity_type, $bundle) {

  switch ($feature) {

    case 'contextual_block':
      gcc_field_create_field_instance(GCC_CONTEXTUAL_BLOCK_FIELD_TYPE, GCC_CONTEXTUAL_BLOCK_FIELD, t('Custom Block Layout'), $entity_type, $bundle);
      break;
  }
}

/**
 * Implements hook_gcc_features_disabled().
 */
function gcc_contextual_block_gcc_features_disable($feature, $entity_type, $bundle) {

  switch ($feature) {

    case 'contextual_block':
      gcc_field_delete_field_instance(GCC_CONTEXTUAL_BLOCK_FIELD, $entity_type, $bundle);
      break;
  }
}

/**
 * Implements hook_gcc_permission().
 */
function gcc_contextual_block_gcc_permission() {

  $perm = array();

  $perm['administer contextual block'] = array(

    'title' => t('Administer Contextual Block'),
    'group' => t('Administration'),
  );

  return $perm;
}
