<?php

/**
 * @file
 * Install, update, and uninstall functions for the group module.
 */

/**
 * Implements hook_field_schema().
 */
function gcc_field_schema($field) {

  $columns = array();
  $indexes = array();

  drupal_load('module', 'gcc');

  switch ($field['type']) {

    case GCC_FIELD_TYPE_ENABLE:
      $columns = array(

        'value' => array(

          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
          'unsigned' => TRUE,
          'default' => 0,
        ),
        'uid' => array(

          'type' => 'int',
          'not null' => TRUE,
          'unsigned' => TRUE,
        ),
      );
      break;

    case GCC_FIELD_TYPE_AUDIENCE:
      $columns = array(

        'entity_type' => array(

          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ),
        'entity_id' => array(

          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      );
      break;
  }

  return array(

    'columns' => $columns,
    'indexes' => $indexes,
  );
}

/**
 * Implements hook_schema().
 */
function gcc_schema() {

  $schema = array();

  $schema['gcc_role'] = array(

    'description' => 'Stores group roles.',
    'fields' => array(

      'rid' => array(

        'description' => 'The machine-readable name of this role.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_type' => array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'bundle' => array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_id' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('rid'),
    'indexes' => array(
      'search' => array('entity_type', 'entity_id'),
      'search2' => array('entity_type', 'bundle'),
    ),
  );

  $schema['gcc_users_roles'] = array(

    'description' => 'Maps users to roles.',
    'fields' => array(

      'uid' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'rid' => array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_type' => array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_id' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('uid', 'rid', 'entity_type', 'entity_id'),
    'indexes' => array(
      'rid' => array('rid'),
      'search' => array('uid', 'entity_type', 'entity_id'),
    ),
  );

  $schema['gcc_role_permission'] = array(

    'description' => 'Stores the permissions assigned to user roles.',
    'fields' => array(

      'rid' => array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_type' => array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_id' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'permission' => array(

        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('rid', 'entity_type', 'entity_id', 'permission'),
  );

  $schema['gcc_membership'] = array(

    'description' => 'Maps users to group.',
    'fields' => array(

      'uid' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity_type' => array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'entity_id' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'state' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'date' => array(

        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('uid', 'entity_type', 'entity_id'),
    'indexes' => array(

      'uid' => array('uid'),
      'group' => array('entity_type', 'entity_id'),
      'state' => array('state'),
    ),
  );

  return $schema;
}

/* Updates */

/**
 * Implements hook_update_N().
 *
 * Change the rid column from serial to varchar.
 */
function gcc_update_7101() {

  db_change_field('gcc_role', 'rid', 'rid', array(
    'type' => 'varchar',
    'length' => 32,
    'not null' => TRUE,
  ));
  db_drop_primary_key('gcc_role');
  db_add_primary_key('gcc_role', array('rid'));

  db_drop_primary_key('gcc_users_roles');
  db_drop_index('gcc_users_roles', 'search');
  db_drop_index('gcc_users_roles', 'rid');
  db_change_field('gcc_users_roles', 'rid', 'rid', array(
    'type' => 'varchar',
    'length' => 32,
    'not null' => TRUE,
  ), array(
    'primary key' => array('uid', 'rid', 'entity_type', 'entity_id'),
    'indexes' => array(
      'rid' => array('rid'),
      'search' => array('uid', 'entity_type', 'entity_id'),
    ),
  ));

  db_drop_primary_key('gcc_role_permission');
  db_change_field('gcc_role_permission', 'rid', 'rid', array(
    'type' => 'varchar',
    'length' => 32,
    'not null' => TRUE,
  ), array(
    'primary key' => array('rid', 'entity_type', 'entity_id', 'permission'),
  ));

  db_update('gcc_role')->fields(array('rid' => GCC_NON_MEMBER))->condition('rid', -3)->execute();
  db_update('gcc_role')->fields(array('rid' => GCC_MEMBER))->condition('rid', -2)->execute();
  db_update('gcc_role')->fields(array('rid' => GCC_ADMIN))->condition('rid', -1)->execute();

  db_update('gcc_users_roles')->fields(array('rid' => GCC_NON_MEMBER))->condition('rid', -3)->execute();
  db_update('gcc_users_roles')->fields(array('rid' => GCC_MEMBER))->condition('rid', -2)->execute();
  db_update('gcc_users_roles')->fields(array('rid' => GCC_ADMIN))->condition('rid', -1)->execute();

  db_update('gcc_role_permission')->fields(array('rid' => GCC_NON_MEMBER))->condition('rid', -3)->execute();
  db_update('gcc_role_permission')->fields(array('rid' => GCC_MEMBER))->condition('rid', -2)->execute();
  db_update('gcc_role_permission')->fields(array('rid' => GCC_ADMIN))->condition('rid', -1)->execute();
}

/**
 * Implements hook_update_N().
 *
 * Increase the size of the columns rid.
 */
function gcc_update_7102() {

  db_drop_primary_key('gcc_role');
  db_change_field('gcc_role', 'rid', 'rid', array(
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
  ), array(
    'primary key' => array('rid'),
  ));

  db_drop_primary_key('gcc_users_roles');
  db_drop_index('gcc_users_roles', 'search');
  db_drop_index('gcc_users_roles', 'rid');
  db_change_field('gcc_users_roles', 'rid', 'rid', array(
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
  ), array(
    'primary key' => array('uid', 'rid', 'entity_type', 'entity_id'),
    'indexes' => array(
      'rid' => array('rid'),
      'search' => array('uid', 'entity_type', 'entity_id'),
    ),
  ));

  db_drop_primary_key('gcc_role_permission');
  db_change_field('gcc_role_permission', 'rid', 'rid', array(
    'type' => 'varchar',
    'length' => 255,
    'not null' => TRUE,
  ), array(
    'primary key' => array('rid', 'entity_type', 'entity_id', 'permission'),
  ));
}

/**
 * Implements hook_update_N().
 *
 * Add the new bundle column.
 */
function gcc_update_7103(&$sandbox) {

  if (!isset($sandbox['step'])) {
    $sandbox['step'] = 'add_field';
  }

  drupal_load('module', 'gcc');

  switch ($sandbox['step']) {

    case 'add_field':
      db_add_field('gcc_role', 'bundle', array(

        'type' => 'varchar',
        'length' => 32,
      ));

      $sandbox['step'] = 'update_global';
      $sandbox['#finished'] = 0.1;
      break;

    case 'update_global':
      db_update('gcc_role')->fields(array(
        'bundle' => GCC_GLOBAL_BUNDLE,
      ))->condition('entity_type', GCC_GLOBAL_TYPE)
      ->condition('entity_id', GCC_GLOBAL_ID)
      ->execute();

      $sandbox['step'] = 'update_local';
      $sandbox['#finished'] = 0.1;
      break;

    case 'update_local':
      if (!isset($sandbox['total'])) {

        $sandbox['total'] = db_select('gcc_role')
        ->condition('entity_type', GCC_GLOBAL_TYPE, '!=')
        ->condition('entity_id', GCC_GLOBAL_ID, '!=')
        ->countQuery()->execute()->fetchField();

        $sandbox['current'] = 0;
      }

      $items = db_select('gcc_role', 'r')->fields('r')
      ->condition('entity_type', GCC_GLOBAL_TYPE, '!=')
      ->condition('entity_id', GCC_GLOBAL_ID, '!=')
      ->range($sandbox['current'], 5)
      ->execute();

      $sandbox['current'] += 5;

      foreach ($items as $item) {

        $entity = entity_load($item->entity_type, array($item->entity_id));
        if (isset($entity[$item->entity_id])) {

          list(,,$bundle) = entity_extract_ids($item->entity_type, $entity[$item->entity_id]);
          db_update('gcc_role')->fields(array(
            'bundle' => $bundle,
          ))->condition('entity_type', $item->entity_type)
          ->condition('entity_id', $item->entity_id)
          ->execute();
        }
      }

      if ($sandbox['current'] > $sandbox['total']) {
        $sandbox['step'] = 'index';
      }
      $sandbox['#finished'] = 0.1;
      break;

    case 'index':
      db_change_field('gcc_role', 'bundle', 'bundle', array(

        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ), array(
        'indexes' => array(
          'search2' => array('entity_type', 'bundle'),
        ),
      ));
      break;
  }
}
