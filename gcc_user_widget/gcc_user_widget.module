<?php

/**
 * @file
 * User widget module.
 */


/**
 * Implements hook_permission().
 */
function gcc_user_widget_permission() {

  $permissions = array();

  $permissions['use gcc user widget'] = array(

    'title' => t('Use the GCC User widget'),
    'description' => t('Use the GCC User widget.'),
  );

  return $permissions;
}

/**
 * Implements hook_menu().
 */
function gcc_user_widget_menu() {

  $items = array();

  $items['gcc-user-widget/%user'] = array(

    'title' => 'GCC User Widget',
    'access callback' => 'user_access',
    'access arguments' => array('use gcc user widget'),
    'page callback' => 'gcc_user_widget_ajax',
    'page arguments' => array(1),
    'delivery callback' => 'ajax_deliver',
    'theme callback' => 'ajax_base_page_theme',
  );

  return $items;
}

/**
 * Implements hook_preprocess_username().
 */
function gcc_user_widget_preprocess_username(&$variables) {

  global $user;

  if (!user_access('use gcc user widget')) {
    return;
  }

  $groups = gcc_membership_get_by_permission($user->uid, 'administer group members', TRUE, FALSE, FALSE);

  if (empty($groups)) {
    return;
  }

  $account = $variables['account'];

  if (!$account->uid) {
    return;
  }

  $id = drupal_html_id('gcc-user-widget');

  $settings = array(
    'gccUserWidget' => array(
      $id => array(

        'uid' => $account->uid,
        'path' => url('gcc-user-widget/' . $account->uid),
      ),
    ),
  );

  drupal_add_js($settings, 'setting');
  drupal_add_js(drupal_get_path('module', 'gcc_user_widget') . '/gcc_user_widget.js');

  drupal_add_library('system', 'drupal.ajax');
  drupal_add_library('system', 'jquery.once');

  drupal_add_css(drupal_get_path('module', 'gcc_user_widget') . '/gcc_user_widget.css');

  $variables['extra'] .= '<span id="' . $id . '" class="gcc-user-widget">' . t('Add to groups') . '</span>';
}

/**
 * Page callback.
 */
function gcc_user_widget_ajax($account) {

  $commands = array();
  $commands[] = ajax_command_remove('.gcc-user-widget-form');
  $commands[] = ajax_command_insert(NULL, drupal_render(drupal_get_form('gcc_user_widget_form', $account)));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Form builder.
 */
function gcc_user_widget_form($form, &$form_state, $account) {

  global $user;

  $form = array();

  $form['#prefix'] = '<div class="gcc-user-widget-form">';
  $form['#sufix'] = '</div>';

  $groups = gcc_membership_get_by_permission($user->uid, 'administer group members', TRUE, FALSE, FALSE);
  $memberships = gcc_membership_get_user_memberships($account->uid, array(

    GCC_ACTIVE,
    GCC_PENDING,
    GCC_BLOCKED,
  ));

  $options = array();
  $default_value = array();
  foreach ($groups as $key => $group) {

    $entity = entity_load($group->entity_type, array($group->entity_id));

    if (isset($entity[$group->entity_id])) {

      $options[$key] = entity_label($group->entity_type, $entity[$group->entity_id]);
      $default_value[$key] = isset($memberships[$key]) ? $key : FALSE;
    }
  }

  $form['groups'] = array(

    '#type' => 'checkboxes',
    '#title' => '',
    '#options' => $options,
    '#default_value' => $default_value,
    '#prefix' => '<div id="gcc-user-widget-checkboxes">',
    '#suffix' => '</div>',
  );

  $form['previous_groups'] = array(

    '#type' => 'value',
    '#value' => $default_value,
  );

  $form['account'] = array(

    '#type' => 'value',
    '#value' => $account,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['apply'] = array(

    '#type' => 'submit',
    '#value' => t('Apply'),
    '#ajax' => array(

      'callback' => 'gcc_user_widget_ajax_callback',
      'wrapper' => 'gcc-user-widget-checkboxes',
      'event' => 'click',
    ),
    '#submit' => array('gcc_user_widget_form_apply'),
  );

  $form['actions']['cancel'] = array(

    '#type' => 'button',
    '#value' => t('Close'),
    '#attributes' => array(
      'class' => array('gcc-user-widget-cancel'),
    ),
  );

  return $form;
}

/**
 * Submit handler.
 */
function gcc_user_widget_form_apply($form, &$form_state) {

  $values = $form_state['values'];

  $form_state['rebuild'] = TRUE;
  $form_state['input'] = array();
  $form_state['values'] = array();

  $previous_groups = array_filter($values['previous_groups']);
  $new_groups = array_filter($values['groups']);

  foreach (array_keys($previous_groups) as $group) {
    if (!isset($new_groups[$group])) {

      list($entity_type, $entity_id) = explode(':', $group);
      gcc_membership_delete($entity_type, $entity_id, $values['account']->uid);
    }
  }

  foreach (array_keys($new_groups) as $group) {
    if (!isset($previous_groups[$group])) {

      list($entity_type, $entity_id) = explode(':', $group);
      gcc_membership_create($entity_type, $entity_id, $values['account']->uid, GCC_ACTIVE);
    }
  }
}

/**
 * Ajax callback.
 */
function gcc_user_widget_ajax_callback($form, $form_state) {

  return $form['groups'];
}
